package qianbao.com.caipiaotouzhuzhan;

import android.app.Application;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by liuyong
 * Data: 2018/6/21
 * Github:https://github.com/MrAllRight
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
    }
}
