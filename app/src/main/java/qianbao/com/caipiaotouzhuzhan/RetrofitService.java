package qianbao.com.caipiaotouzhuzhan;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by liuyong
 * Data: 2018/7/3
 * Github:https://github.com/MrAllRight
 */

public interface RetrofitService {
    @GET("ios1_1_cptzz.jsp")
    Call<ResponseBody> getGameList();
}
