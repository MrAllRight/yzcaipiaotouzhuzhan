package qianbao.com.caipiaotouzhuzhan;

/**
 * Created by liuyong
 * Data: 2018/7/3
 * Github:https://github.com/MrAllRight
 */

public class Entity {
    /**
     * retCode : 1
     * retMsg : http://web2.goodview.info:8101/
     */

    private String retCode;
    private String retMsg;

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }
}
